<?php

/**
 * Classe principale de l'application
 *
 * @package    App
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

use App\Db;
use App\Router;

class App {

	/** 
	 * Lancement de l'application.
	 */
	public static function run() {

		// Connexion à la base de données
		Db::connect();

		// Appel du dispatcher pour appeller le contrôleur et son action
		Router::dispatch();
		
	}

}