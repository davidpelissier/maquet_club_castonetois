<?php

/**
 * Contrôleur maître de l'application
 *
 * Tous les autres contrôleurs de l'application doivent hériter de cette classe.
 *
 * @package    App\Controllers
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

namespace App;

use App\AppView;

class AppCtrl {

	/** Nom du contrôleur */
	public $name;

	/** Modèle du contrôleur */
	public $model;

	/** Action du contrôleur */
	public $action = 'index';

	/** Vue du contrôleur */
	public $view;

	/** Constructeur par défaut */
	public function __construct() {
		$this->name  = strtolower(substr(get_class($this), 16, -4));
		$this->model = NULL;
		$this->view  = new AppView($this->name, $this->action);
	}

}