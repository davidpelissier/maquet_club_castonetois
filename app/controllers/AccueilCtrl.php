<?php

/**
 * Contrôleur Accueil
 *
 * @package    App\Controllers
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

namespace App\Controllers;

use App\AppCtrl;

class AccueilCtrl extends AppCtrl {

	public function index() {


	}

}