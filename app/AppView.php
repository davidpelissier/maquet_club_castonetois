<?php

/**
 * Vue maîtresse de l'application
 *
 * @package    App\Views
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

namespace App;

class AppView {

	/** 
	 * Layout de la vue.
	 *
	 * @var String Nom du thème par défaut de la vue.
	 */
	public $layout;

	/** 
	 * Fichier de la vue.
	 *
	 * @var String Nom du thème par défaut de la vue.
	 */
	public $file;

	/** 
	 * Variables de la vue.
	 * 
	 * @var Tableau associatif $var => $value qui permet de définir et d'associer des variables à
	 * leur valeur dans la vue.
	 */
	public $vars;

	/**
	 * Contrôleur par défaut.
	 */
	public function __construct($controller, $action) {

		$this->layout = 'default';
		$this->file   = APP . DS . 'app' . DS . 'views' . DS . $controller . DS . $action . '.php';
		$this->vars   = [];

		if (!file_exists($this->file)) {
			echo '<pre>Vue inexistante</pre>';
		}
	}

	/**
	 * Méthode de rendu de la vue.
	 */
	public function render() {

		extract($this->vars);
		ob_start();
		include $this->file;
		return ob_end_flush();
	}

	/**
	 * Méthode permettant de définir et d'associer une valeur à une variable de la vue.
	 *
	 * @param String $var 	Nom de la variable à définir
	 * @param Object $value Valeur de la variable
	 * @return void
	 */
	public function setVar($var, $value) {
		$this->vars[$var] = $value;
	}

}