<?php

/**
 * Classe d'accès et de management de la base de données de l'application
 *
 * @package    App\Db
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

namespace App;

use \PDO;
use App\Config;

class Db {

	/**
	 * Connexion à une base ODBC (spécifiée dans le fichier de configuration) avec l'invocation de 
	 * pilote sous forme d'un objet PDO.
	 *
	 * @return PDO Instance de PDO si la database configurée existe. NULL sinon.
	 * @see App\Config
	 */
	public static function connect() {		
		
		// Connexion à une base ODBC avec l'invocation de pilote
		$dsn      = 'mysql:dbname='. Config::$database .';host=' . Config::$host;
		$user     = Config::$username;
		$password = Config::$password;

		try {
		    $dbh = new PDO($dsn, $user, $password);
		} catch (PDOException $e) {
		    echo 'Connexion echouee : <br /><pre>'. $e->getMessage() .'</pre>';
		    $dbh = NULL;
		}
		
		return $dbh;
	}

}