<?php

/**
 * Classe de routage de l'application.
 *
 * Permet de définir le contrôleur et l'action de ce dernier à appeller.
 *
 * @package    App\Router
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

namespace App;

use App\AppCtrl;
use App\AppView;

class Router {

	/**
	 * Détermine le contrôleur et l'action à appeller et instancie les classes nécessaire en fonction.
	 *
	 * @return PDO Instance de PDO si la database configurée existe. NULL sinon.
	 * @see App\Config
	 */
	public static function dispatch() {		
		
		// On  récupère les paramètres de la requête
		if (isset($_GET['controller'])) {
			$controller = 'App\\Controllers\\'. ucfirst($_GET['controller']) .'Ctrl';
		} else {
			$controller = 'App\\Controllers\\AccueilCtrl';
		}
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		} else {
			$action = 'index';
		}
		if (isset($_GET['id'])) {
			$id = $_GET['id'];
		}

		// On instancie le contrôleur s'il existe
		if (class_exists($controller)) {
			$controller = new $controller();
		} else {
			echo '<pre>Contrôleur ' . $controller . ' inexistant</pre>';
			exit;
		}

		// On appelle l'action si elle existe
		if(method_exists($controller, $action)) {
			$controller->action = $action;
		} else {
			echo '<pre>Action ' . $action . ' inexistante</pre>';
			exit;
		}

		$controller->$action();
		$controller->view->render();
	}

}