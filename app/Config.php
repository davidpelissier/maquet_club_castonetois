<?php

/**
 * Classe de configuration de l'application
 *
 * @package    App\Config
 * @author     David PELISSIER<david.pelisier@laposte.net>
 * @author     Valentin MOLIERES<valentin.molieres@gmail.com>
 * @copyright  Copyrights 2015 · David PELISSIER & Valentin MOLIERES
 * @version    0.1
 */

namespace App;

class Config {

	/** Hôte de la base de données */
	public static $host = 'localhost';
	
	/** Nom de la base de données */
	public static $database = 'maquet_club_castenois';
	
	/** Nom d'utilisateur de connexion à la base de données */
	public static $username = 'root';
	
	/** Mot de passe de connexion à la base de données */
	public static $password = '';
	
	/** Port de connexion à la base de données */
	public static $port = '';
	
	/** Encodage de la base de données */
	public static $encoding = 'utf-8';

}