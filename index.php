<?php

define('APP', __DIR__);
define('DS', DIRECTORY_SEPARATOR);

// Chargement automatique des classes appellées
function __autoload($class) {

	$package = explode(DS, str_replace('\\', DS, $class)) ;
	$class   = array_pop($package);
	$file    = '';
	foreach ($package as $folder) {
		$file .= strtolower($folder) . DS;
	}
	$file .= $class;

	if ( file_exists($file . '.php') ) {
        require_once $file . '.php';
	}
}

// Chargement de la classe principale de l'application
require_once 'app/App.php';

App::run();